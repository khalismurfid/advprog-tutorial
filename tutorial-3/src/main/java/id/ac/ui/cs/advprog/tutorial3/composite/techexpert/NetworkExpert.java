package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class NetworkExpert extends Employees {

    public NetworkExpert(String name, double salary) {
        this.role = "Network Expert";
        this.name = name;
        if (salary < 50000) {
            throw new IllegalArgumentException("Network Expert Salary must not lower than 50000");
        }
        this.salary = salary;
    }

    @Override
    public double getSalary() {
        return salary;
    }
}

