package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Ceo extends Employees {
    public Ceo(String name, double salary) {
        this.role = "CEO";
        this.name = name;
        if (salary < 200000) {
            throw new IllegalArgumentException("CEO Salary must not lower than 200000");
        } else {
            this.salary = salary;
        }
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
