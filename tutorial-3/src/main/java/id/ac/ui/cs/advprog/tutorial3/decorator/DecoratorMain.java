package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class DecoratorMain {

    public static void main(String[] args) {
        Food thickBunBurgerSpecial = BreadProducer.THICK_BUN.createBreadToBeFilled();

        thickBunBurgerSpecial = FillingDecorator.BEEF_MEAT.addFillingToBread(thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.getDescription());

        thickBunBurgerSpecial = FillingDecorator.CHEESE.addFillingToBread(thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.getDescription());

        thickBunBurgerSpecial = FillingDecorator.CUCUMBER.addFillingToBread(thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.getDescription());

    }


}

