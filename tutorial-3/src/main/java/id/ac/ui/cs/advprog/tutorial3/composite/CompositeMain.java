package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;


public class CompositeMain {

    public static void main(String[] args) {
        Company company = new Company();

        Employees luffy = new Ceo("Luffy", 500000.00);
        company.addEmployee(luffy);

        Employees zorro = new Cto("Zorro", 320000.00);
        company.addEmployee(zorro);

        Employees franky = new BackendProgrammer("Franky", 94000.00);
        company.addEmployee(franky);

    }
}

