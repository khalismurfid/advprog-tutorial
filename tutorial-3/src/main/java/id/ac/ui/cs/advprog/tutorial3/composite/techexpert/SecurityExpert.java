package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class SecurityExpert extends Employees {

    public SecurityExpert(String name, double salary) {
        this.role = "Security Expert";
        this.name = name;
        this.salary = salary;
        if (salary < 70000) {
            throw new IllegalArgumentException("Back End Programmer Salary must not lower than 70000");
        }
        this.salary = salary;
    }

    @Override
    public double getSalary() {
        return salary;
    }
}

