package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class FrontendProgrammer extends Employees {

    public FrontendProgrammer(String name, double salary) {
        this.role = "Front End Programmer";
        this.name = name;
        if (salary < 30000) {
            throw new IllegalArgumentException("FrontendProgrammer Salary must not lower than 30000");
        }
        this.salary = salary;
    }


    @Override
    public double getSalary() {
        return salary;
    }
}

