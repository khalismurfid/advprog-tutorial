package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class UiUxDesigner extends Employees {

    public UiUxDesigner(String name, double salary) {
        this.role = "UI/UX Designer";
        this.name = name;
        if (salary < 90000) {
            throw new IllegalArgumentException("UI/UX Designer Salary must not lower than 90000");
        }
        this.salary = salary;


    }


    @Override
    public double getSalary() {
        return salary;
    }
}

