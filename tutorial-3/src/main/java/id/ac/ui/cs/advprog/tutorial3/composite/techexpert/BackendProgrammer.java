package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class BackendProgrammer extends Employees {

    public BackendProgrammer(String name, double salary) {
        this.role = "Back End Programmer";
        this.name = name;
        if (salary < 20000) {
            throw new IllegalArgumentException("Back End Programmer Salary must not lower than 20000");
        } else {
            this.salary = salary;
        }
    }

    @Override
    public double getSalary() {
        return salary;
    }
}

