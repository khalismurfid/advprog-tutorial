package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;


public class MacroCommand implements Command {

    private List<Command> commands;

    public MacroCommand(Command[] commands) {
        this.commands = Arrays.asList(commands);
    }

    @Override
    public void execute() {
        commands.stream().forEach(command -> command.execute());
        // TODO Complete me!
    }

    @Override
    public void undo() {
        commands.stream().collect(Collectors.toCollection(LinkedList::new))
            .descendingIterator().forEachRemaining(command -> command.undo());


        // TODO Complete me!
    }
}
